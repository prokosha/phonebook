class PhoneBook {
    constructor(selector) {
        this.selector = '.phone_book' + selector;
        this.list = document.querySelector(this.selector + " .list_contacts");
        this.formChange = document.querySelector(this.selector + " .change_contact .form_Ph-B");
        this.form = document.querySelectorAll(this.selector + " .form_Ph-B");
        this.bnChange = document.querySelector(this.selector + " .button_change");
        this.listBirthDayPerson = document.querySelector(this.selector + " .birthDayContacts");
        this.photoChange = document.querySelector(this.selector + " .change_contact .photoContact");
        this.tmpl = _.template(document.querySelector(this.selector + ' .tmpl').innerHTML);
        this.tmplContacts = _.template(document.querySelector(this.selector + ' .tmpl-contacts').innerHTML);
        this.tmplAddNumber = document.getElementById('tmpl_addNumber');
        this.bnRemove = document.querySelector(this.selector + " .change_contact .button_remove");
        this.bnAddPhoto = document.querySelectorAll(this.selector + " .fl_inp");
        this.searchInput = document.getElementById("mySearch");
        this.checkboxSelectAll = document.querySelector(this.selector + " .checked");
        this.bnAddPhoto.forEach(el => el.addEventListener('change', this.addphoto.bind(this)));
        document.querySelector(this.selector + ' .button_add').addEventListener('click', this.addContact.bind(this));
        document.querySelector(this.selector + ' .del_concact').addEventListener('click', this.delete.bind(this));
        this.list.addEventListener('click', this.showContact.bind(this));
        this.bnChange.addEventListener('click', this.changeContact.bind(this));
        this.bnRemove.addEventListener('click', this.deleteContact.bind(this));
        this.form.forEach(el => el.addEventListener('click', this.buttonNumberPhone.bind(this)));
        document.querySelector(this.selector + " .contacts").addEventListener('click', this.stateCheckboxSelectAll.bind(this));
        const dataList = {
            list: [],
            countID: 0
        }
        const localStor = localStorage.getItem('localList');
        this.localList = localStor ? JSON.parse(localStor) : dataList;
        this.change = false;
        this.targetID = 0;
        this.loadLocalStor();
        this.getBirthdayContacts();
        this.awesomplete = new Awesomplete(this.searchInput, {
            minChars: 1,
            autoFirst: true,
            maxItems: 10
        });
        this.awesomplete.list = this.getListNameContact();
        document.querySelector(this.selector + " .bn-search").addEventListener('click', this.searchContact.bind(this));
    }
    setToLocalStor() {
        localStorage.setItem('localList', JSON.stringify(this.localList));
        this.loadLocalStor();
        this.getBirthdayContacts();
        this.awesomplete.list = this.getListNameContact();
    }
    createItem(sel, i) {
        const nameContact = document.querySelector(this.selector + " ." + sel + " .name_contact");
        const birthDay = document.querySelector(this.selector + " ." + sel + " .birthDay_contact");
        const phoneNumbers = document.querySelectorAll(this.selector + " ." + sel + " .number_contact");
        const photo = document.querySelector(this.selector + " ." + sel + " .photoContact");
        const valNumContact = [];
        if (sel === "add_contact") {
            const index = this.localList.list.push({ id: "id" + i, name: "", number: 0, birthDay: 0, photo: "" });
            this.localList.countID++;
            i = index - 1;
        }
        phoneNumbers.forEach(el => (el.value === "") ? el.parentNode.remove() : valNumContact.push(el.value));
        this.localList.list[i].name = nameContact.value;
        this.localList.list[i].number = valNumContact;
        this.localList.list[i].birthDay = birthDay.value;
        this.localList.list[i].photo = photo.src;
        this.localList.list.sort((a, b) => a.name.localeCompare(b.name));
        this.setToLocalStor();
    }
    addContact() {
        this.createItem("add_contact", this.localList.countID);
        this.resetForm("add_contact");
    }
    addNumberPhone(e, sel) {
        if (e !== null) { e.preventDefault() };
        const listContacts = document.querySelector(this.selector + " ." + sel + " .cont_number_phone");
        listContacts.appendChild(this.tmplAddNumber.content.cloneNode(true));
    }
    buttonNumberPhone(e) {
        let { parentElement } = e.target;
        if (e.target.classList.contains("del_more-number")) {
            parentElement.remove();
        } else if (e.target.classList.contains("add_numberPH")) {
            this.addNumberPhone(e, e.target.form.name);
        }
    }
    showContact({ target }) {
        if (this.change) {
            this.bnChange.textContent = "Изменить";
            this.change = false;
        }
        this.removeExcessElem("change_contact");
        const changeNameContact = document.querySelector(this.selector + " .change_contact .name_contact");
        const birthDayAbonent = document.querySelector(this.selector + " .change_contact .birthDay_contact");
        this.targetID = target.dataset.id;
        const curIndex = this.findIndexContact();
        if (curIndex < 0) {
            return;
        }
        changeNameContact.value = this.localList.list[curIndex].name;
        birthDayAbonent.value = this.localList.list[curIndex].birthDay;
        this.photoChange.src = this.localList.list[curIndex].photo;
        this.localList.list[curIndex].number.forEach((el, i) => {
            this.addNumberPhone(null, "change_contact");
            const num = document.querySelectorAll(this.selector + " .change_contact .number_contact");
            num[i].value = el;
        });
        this.toDoDisabled(true);
    }
    removeExcessElem(sel) {
        const listNumPhone = document.querySelector(this.selector + " ." + sel + " .cont_number_phone");
        listNumPhone.textContent = "";
    }
    findIndexContact() {
        return this.localList.list.findIndex(el => el.id === this.targetID);
    }
    toDoDisabled(key) {
        [...this.formChange.elements].forEach(el => {
            if (!el.classList.contains('button_change') && !el.classList.contains('button_remove')) {
                el.disabled = key;
            }
        });
    }
    changeContact(e) {
        e.preventDefault();
        if (!this.change) {
            this.bnChange.textContent = "Сохранить";
            this.toDoDisabled(false);
            this.change = !this.change;
        } else {
            this.bnChange.textContent = "Изменить";
            const index = this.findIndexContact();
            if (index < 0) {
                return;
            }
            this.createItem("change_contact", index);
            this.change = false;
            this.toDoDisabled(true);
        }
    }
    resetForm(sel) {
        const formCur = document.querySelector(this.selector + " ." + sel + " .form_Ph-B");
        const photo = document.querySelector(this.selector + " ." + sel + " .photoContact");
        formCur.reset();
        photo.src = "";
        this.removeExcessElem(sel);
    }
    deleteContact(e) {
        e.preventDefault();
        const indexDel = this.findIndexContact();
        if (indexDel < 0) {
            return;
        }
        this.localList.list.splice(indexDel, 1);
        this.setToLocalStor();
        this.resetForm("change_contact");
    }
    getBirthdayContacts() {
        this.listBirthDayPerson.textContent = "";
        const date = new Date();
        const month = date.getMonth() + 1;
        const monthToday = (date.getMonth() < 10) ? '0' + month : month;
        const today = date.getFullYear() + '-' + monthToday + '-' + date.getDate();
        this.localList.list.forEach(el => {
            if (el.birthDay === today) {
                const value = el.name;
                this.listBirthDayPerson.innerHTML += this.tmpl({ value });
            }
        });
    }
    loadLocalStor(valueSearch = "") {
        this.list.textContent = "";
        const valInput = valueSearch.replace(/\s+/g, '').toLocaleLowerCase();
        if (this.localList.list.length > 0) {
            this.localList.list.forEach(el => {
                const str = el.name.replace(/\s+/g, '').toLocaleLowerCase();
                if (~str.indexOf(valInput) || valueSearch === "") {
                    const nameContact = el.name;
                    const idContact = el.id;
                    this.list.innerHTML += this.tmplContacts({ nameContact, idContact });
                }
            });
        }
    }
    addphoto({ target }) {
        const sel = target.form.name;
        const photo = document.querySelector(this.selector + " ." + sel + " .photoContact");
        const { value } = document.querySelector(this.selector + " ." + sel + " .fl_inp");
        const nameFile = value.replace(/.*\\/, "");
        photo.src = "img/" + nameFile;
    }
    getListNameContact() {
        return this.localList.list.map(el => el.name);
    }
    searchContact() {
        const { value } = this.searchInput;
        this.loadLocalStor(value);
        this.resetListContacts();
    }
    resetListContacts() {
        this.checkboxSelectAll.checked = false;
        this.searchInput.value = "";
        this.resetForm("change_contact");
    }
    stateCheckboxSelectAll({ target }) {
        const allElem = document.querySelectorAll(this.selector + " .check");
        if (target.classList.contains('check')) {
            const allChecked = document.querySelectorAll(this.selector + " .check:checked").length;
            this.checkboxSelectAll.checked = allChecked === allElem.length;
            this.checkboxSelectAll.indeterminate = allChecked > 0 && allChecked < allElem.length;
        } else if (target.classList.contains('checked')) {
            allElem.forEach(el => el.checked = this.checkboxSelectAll.checked);
        }
    }
    delete() {
        const listChecked = document.querySelectorAll(this.selector + " .check:checked");
        for (let i = 0; i < this.localList.list.length; i++) {//forEach не срабатывал на посл. эл., поэтому исп. for
            listChecked.forEach(elCheck => {
                if (this.localList.list[i].id === elCheck.id) {
                    this.localList.list.splice(i, 1);
                }
            });
        }
        this.resetListContacts();
        this.setToLocalStor();
    }
}
new PhoneBook(".example1");